import os

CURRENT_FOLDER=""
LIST_OF_SUBFOLDERS=["cpp_common/", "external/rapidjson/", "picker_controller/", "planner/", "simulator/"]
LIST_OF_EXTENSIONS=["*.cpp", "*.cxx", "*.cc", "*.c", "*.h", "*.hh", "*.hpp"]
NAME_OF_CSCOPE_FILE="./cscope.files"

start_command = f"echo \'\' > {NAME_OF_CSCOPE_FILE}"
os.system(start_command)

for sf in LIST_OF_SUBFOLDERS:
    s = CURRENT_FOLDER + sf
    for e in LIST_OF_EXTENSIONS:
        command = f"find {s} -name {e} >> {NAME_OF_CSCOPE_FILE}"
        os.system(command)


os.system("cscope -b -q -k")